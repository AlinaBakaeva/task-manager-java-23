package com.bakaeva.tm.exception.system;

public final class NotNumberException extends RuntimeException {

    public NotNumberException(String value) {
        super("Error! This value ``"+ value +"`` is not a number...");
    }

}