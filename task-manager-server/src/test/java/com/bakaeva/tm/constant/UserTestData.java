package com.bakaeva.tm.constant;

import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.util.HashUtil;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static User USER3 = new User();

    @NotNull
    public final static User ADMIN1 = new User();

    @NotNull
    public final static List<User> USER_LIST = Arrays.asList(USER1, USER2, USER3, ADMIN1);

    static {
        for (int i = 0; i < USER_LIST.size(); i++) {
            @NotNull final User user = USER_LIST.get(i);
            user.setId("u-0" + i);
            user.setLogin("user" + i);
            @Nullable final String passwordHash = HashUtil.salt(user.getLogin());
            if (passwordHash != null) user.setPasswordHash(passwordHash);
            user.setEmail("user" + i + "@us.er");
            user.setFirstName("Name" + i);
            user.setLastName("Last-Name" + i);
            user.setMiddleName("Middle-Name" + i);
            user.setRole(Role.USER);
        }
        ADMIN1.setRole(Role.ADMIN);
    }

}