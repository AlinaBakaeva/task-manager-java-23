package com.bakaeva.tm.util;

import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

import static com.bakaeva.tm.constant.UserTestData.ADMIN1;

public final class SignatureUtilTest {

    @Test
    public void signObject() {
        @Nullable final String result1 = SignatureUtil.sign(ADMIN1, "salt", 1002);
        @Nullable final String result2 = SignatureUtil.sign(ADMIN1, "salt", 1002);
        Assert.assertNotNull(result2);
        Assert.assertEquals(result1, result2);
    }

    @Test
    public void signString() {
        @Nullable final String result1 = SignatureUtil.sign("test{String-12345", "salt", 1002);
        @Nullable final String result2 = SignatureUtil.sign("test{String-12345", "salt", 1002);
        Assert.assertNotNull(result2);
        Assert.assertEquals(result1, result2);
    }

}