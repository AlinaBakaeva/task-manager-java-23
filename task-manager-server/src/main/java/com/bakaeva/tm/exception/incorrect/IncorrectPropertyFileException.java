package com.bakaeva.tm.exception.incorrect;

public final class IncorrectPropertyFileException extends RuntimeException {

    public IncorrectPropertyFileException(String value) {
        super("Error! Incorrect property file ``" + value + "``...");
    }

}