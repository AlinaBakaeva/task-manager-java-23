package com.bakaeva.tm.exception.empty;

public final class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Error! ID  is empty...");
    }

}