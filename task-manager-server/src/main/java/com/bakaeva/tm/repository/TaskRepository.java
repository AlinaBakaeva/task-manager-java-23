package com.bakaeva.tm.repository;

import com.bakaeva.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.api.repository.ITaskRepository;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository{

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        entities.add(task);
        task.setUserId(userId);
    }

    @Override
    public void remove(@NotNull final String userId, final Task task) {
        if (!userId.equals(task.getUserId())) return;
        entities.remove(task);
    }

    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : entities) {
            if (task == null) continue;
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Task> userTasks = findAll(userId);
        entities.removeAll(userTasks);
    }

    @Nullable
    @Override
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        for (@Nullable final Task task : entities) {
            if (task == null) continue;
            if (id.equals((task.getId())) && userId.equals(task.getUserId()))
                return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        for (@Nullable final Task task : entities) {
            if (task == null) continue;
            if (name.equals((task.getName())) && userId.equals(task.getUserId()))
                return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable Task task = findById(userId, id);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

}