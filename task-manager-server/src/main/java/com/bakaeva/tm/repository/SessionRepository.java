package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.ISessionRepository;
import com.bakaeva.tm.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    @Override
    public List<Session> findByUserId(@NotNull final String userId) {
        @NotNull final List<Session> sessions = findAll();
        @NotNull final List<Session> result = new ArrayList<>();
        for (@Nullable final Session session : sessions) {
            if (session == null) continue;
            if (userId.equals(session.getUserId())) result.add((session));
        }
        return result;
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        final List<Session> sessions = findByUserId(userId);
        for (final Session session : sessions) remove(session);
    }

    @Override
    public @Nullable Session findById(@NotNull String id) {
        for (@Nullable final Session session : entities) {
            if (session == null) continue;
            if (id.equals(session.getId()))
                return session;
        }
        return null;
    }

}